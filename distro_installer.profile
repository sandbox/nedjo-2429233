<?php
/**
 * @file
 * Enables installation from a curated list of distributions.
 */

/**
 * Need to do a manual include since this install profile never actually gets
 * installed so therefore its code cannot be autoloaded.
 */
include_once __DIR__ . '/src/Form/SelectDistro.php';
include_once DRUPAL_ROOT . '/core/modules/system/src/Plugin/Archiver/Tar.php';

use Drupal\Core\Archiver\ArchiverManager;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;

/**
 * Implements hook_install_tasks_alter().
 */
function distro_installer_install_tasks_alter(&$tasks, $install_state) {
  // We want to make our insertions after the install_select_locale task.
  $key = array_search('install_select_locale', array_keys($tasks));
  $distro_tasks = array(
    'distro_installer_verify_requirements' => array(
      'display_name' => t('Verify distribution download requirements'),
    ),
    // The install_select_profile task will run and assign our profile
    // automatically (provided no other profile designated 'exclusive' is
    // present). Here we emulate the profile selection form but instead offer
    // distributions.
    'distro_installer_select_distro' => array(
      'display_name' => t('Choose distribution'),
      'run' => INSTALL_TASK_RUN_IF_REACHED,
    ),
    'distro_installer_download_distro' => array(
      'run' => INSTALL_TASK_RUN_IF_REACHED,
    ),
  );

  $tasks = array_slice($tasks, 0, $key + 1, TRUE) +
    $distro_tasks +
    array_slice($tasks, $key, NULL , TRUE);

  // Customize the install_select_profile callback.
  $tasks['install_select_locale']['function'] = 'distro_installer_select_locale';
}

/**
 * Sets the locale to 'en'.
 */
function distro_installer_select_locale(&$install_state) {
  $install_state['parameters']['locale'] = 'en';
}

/**
 * Presents a form for selecting the distribution to install.
 */
function distro_installer_select_distro(&$install_state) {
  if (!empty($_POST['distro'])) {
    $install_state['parameters']['distro'] = $_POST['distro'];
  }
  elseif (empty($install_state['parameters']['distro']) && $install_state['interactive']) {
    return install_get_form('Drupal\distro_installer\Form\SelectDistro', $install_state);
  }

}

/**
 * Verifies the requirements for downloading and extracting a distribution.
 *
 * The install_verify_requirements() function doesn't invoke an install
 * profile's hook_requirements(), so we need to provide this customized
 * replacement.
 *
 * @param $install_state
 *   An array of information about the current installation state.
 *
 * @return
 *   A themed status report, or an exception if there are requirement errors.
 *   If there are only requirement warnings, a themed status report is shown
 *   initially, but the user is allowed to bypass it by providing 'continue=1'
 *   in the URL. Otherwise, no output is returned, so that the next task can be
 *   run in the same page request.
 *
 * @see http://www.drupal.org/node/1971072
 */
function distro_installer_verify_requirements(&$install_state) {
  // Check the installation requirements for Drupal and this profile.
  require_once DRUPAL_ROOT . '/profiles/distro_installer/distro_installer.install';
  $requirements = distro_installer_requirements('install');

  return install_display_requirements($install_state, $requirements);
}

/**
 * Downloads and extracts the distro archive.
 */
function distro_installer_download_distro(&$install_state) {
  if (!empty($install_state['parameters']['distro']) ) {
    $distros = distro_installer_get_distros();
    foreach ($distros as $machine_name => $distro) {
      if ($install_state['parameters']['distro'] == $machine_name) {
        $temporary_directory = distro_installer_file_directory_temp();
        $filename = $temporary_directory . '/' . basename($distro['url']);
        copy($distro['url'], $filename);

        // System module is not yet enabled, so we need to explicitly pass its
        // namespace.
        $base_directory = \Drupal::root() . '/core/modules/system/src';
        $namespaces = new \ArrayObject(array('Drupal\system' => $base_directory));

        $discovery = new AnnotatedClassDiscovery('', $namespaces);

        $cache_backend = \Drupal::service('cache.discovery');
        $module_handler = \Drupal::service('module_handler');

        $archiver = new ArchiverManager($namespaces, $cache_backend, $module_handler);

        $archiver = $archiver->getInstance(array('filepath' => $filename));

        if (!$archiver) {
          throw new Exception(t('Cannot extract %file; not a valid archive.', array('%file' => $filename)));
        }
        $profiles_directory = DRUPAL_ROOT . '/profiles';
        $archiver->extract($profiles_directory);
        unset($install_state['parameters']['distro']);
        unset($_GET['distro']);
        $install_state['parameters']['profile'] = $install_state['parameters']['distro'] = $machine_name;

        // Warn about profiles directory permissions risk.
        if (!drupal_verify_install_file($profiles_directory, FILE_NOT_WRITABLE, 'dir')) {
          drupal_set_message(t('All necessary changes to %dir have been made, so you should remove write permissions to it now in order to avoid security risks. If you are unsure how to do so, consult the <a href="@handbook_url">online handbook</a>.', array('%dir' => 'profiles', '@handbook_url' => 'http://drupal.org/server-permissions')), 'warning');
        }

        // Unset the previously set 'en' language so that language selection
        // will be performed for the newly downloaded distribution.
        unset($install_state['parameters']['locale']);
      }
    }
  }
}

/**
 * Returns the distributions included in Distribution Installer.
 *
 * @return array
 *   Array of distribution information keyed by distribution machine name.
 */
function distro_installer_get_distros() {
  $uri = DRUPAL_ROOT . '/profiles/distro_installer/distro_installer.info.yml';
  $info = \Drupal::service('info_parser')->parse($uri);
  return $info['distro_installer']['distros'];
}

/**
 * Returns a writable temp directory.
 *
 * We can't use file_directory_temp() here because that function sets a config
 * object and we don't have a storage yet.
 *
 * @return string
 *   The directory path.
 */
function distro_installer_file_directory_temp() {
  $temporary_directory = \Drupal::config('system.file')->get('path.temporary');
  if (empty($temporary_directory)) {
    $temporary_directory = file_directory_os_temp();

    if (empty($temporary_directory)) {
      // If no directory has been found default to 'files/tmp'.
      $temporary_directory = PublicStream::basePath() . '/tmp';

      // Windows accepts paths with either slash (/) or backslash (\), but will
      // not accept a path which contains both a slash and a backslash. Since
      // the 'file_public_path' variable may have either format, we sanitize
      // everything to use slash which is supported on all platforms.
      $temporary_directory = str_replace('\\', '/', $temporary_directory);
    }

  }

  return $temporary_directory;
}
