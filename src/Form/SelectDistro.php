<?php

/**
 * @file
 * Contains \Drupal\distro_installer\Form\SelectDistro.
 */

namespace Drupal\distro_installer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\HttpCache\StoreInterface;

/**
 * .
 */
class SelectDistro extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'distro_installer_select_distro';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#title'] = $this->t('Select a distribution to install.');

    $distros = distro_installer_get_distros();
    $form['distro'] = array();
    foreach ($distros as $machine_name => $distro) {
      if (empty($distro['logo_path'])) {
        $distro['logo_path'] = 'profiles/distro_installer/images/' . $machine_name . '.png';
      }
      $distro_logo = array();
      $distro_logo['image'] = array(
        '#theme' => 'image',
        '#uri' => $distro['logo_path'],
        '#height' => '80',
        '#width' => '80',
        '#attributes' => array('class' => array('distro-logo')),
        '#alt' => $this->t('Logo for %name distribution', array('%name' => $distro['name'])),
      );
      $distro_logo['description'] = array(
        '#markup' => $distro['description'] . '<span class="clearfix"></span>',
      );
      $form['distro'][$machine_name] = array(
        '#type' => 'radio',
        '#value' => 'standard',
        '#return_value' => $machine_name,
        '#title' => $distro['name'],
        '#description' => $distro_logo,
        '#parents' => array('distro'),
      );
    }
    $form['message'] = array(
      '#markup' => $this->t('Select a distribution to install. This step may take some time while the distribution is downloaded and extracted.'),
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save and continue'),
      '#weight' => 15,
      '#button_type' => 'primary',
    );

    $form['#attached']['library'][] = 'drupal.distro_installer.install';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
